/*global requestAnimationFrame*/
var epg = (function(ns) {
    'use strict';

    ns.DragDrop = function(activityContainerId, options, $) {

        var dragging = null;
        var draggingOffset = {};
        var draggingOriginalOffset = null;
        var dragPos = null;
        var draggingWidth = 0;
        var draggingHeight = 0;
        var doc = $(document);
        var containerId = activityContainerId;
        var targets;
        var draggables;
        var targetBounds;
        var currentTarget = null;
        var answersHaveBeenChecked = false;
        var o;
        var settings = {
            automatic: true,
            instantReturn: false,
            draggingClass: null,
            dragOverClass: null,
            matchSizeOnDrop: false,
            markCorrectClass: null,
            markWrongClass: null,
            snapTo: 'centre' //can be entre or left
        };

        $.extend(settings, options);

        if (!window.requestAnimationFrame) {
            throw 'Requires requestAnimationFrame use a shim for Browsers that dont have it';
        }


        var startDrag = function(eo) {
            var i, t, offsetNow,

                pos = getPosition(eo);
            //one at a time
            if (!dragging) {
                eo.preventDefault();
                dragging = $(this);

                //look though targets and see if it's occupying a spot, remove if it is
                for (i = 0; i < targetBounds.length; i += 1) {
                    t = targetBounds[i];
                    if (t.occupied[0] === dragging[0]) {
                        //in automatic mode remove and continue,
                        if (settings.automatic || !answersHaveBeenChecked) {
                            t.occupied = false;
                            if (settings.matchSizeOnDrop) {
                                t.element.css('width', '');
                                t.element.css('height', '');
                                resetTargetCache();
                            }
                            break;
                        } else {
                            //in manual mode if answers have been checked, lock this item if it's in the correct place
                            if (t.occupied.attr('data-dragdrop-id') === t.element.attr('data-dragdrop-accepts')) {
                                return;
                            } else {
                                t.occupied = false;
                                if (settings.matchSizeOnDrop) {
                                    t.element.css('width', '');
                                    t.element.css('height', '');
                                    resetTargetCache();
                                }
                                break;
                            }
                        }
                    }
                }

                //calculate offset to element under cursor when draggin
                offsetNow = dragging.offset();

                var relx = parseInt(dragging.css('left')) || 0;
                var rely = parseInt(dragging.css('top')) || 0;

                draggingOriginalOffset = {
                    top: offsetNow.top - rely,
                    left: offsetNow.left - relx
                };

                draggingOffset.top = pos.y - offsetNow.top;
                draggingOffset.left = pos.x - offsetNow.left;

                //cache width and height to speed colision detection
                draggingWidth = dragging.outerWidth();
                draggingHeight = dragging.outerHeight();

                //setup minimium css
                dragging.css('position', 'relative');
                dragging.css('zIndex', 1000);

                //let designer add any extras
                if (settings.draggingClass) {
                    dragging.addClass(settings.draggingClass);
                }

                //setup events (on doc to ensure capture)
                doc.on('mousemove touchmove', dragMonitor);
                doc.on('mouseup touchend', stopDrag);

                //make first request to update the screen
                requestAnimationFrame(dragUpdate);
            }
        };

        var resetTargetCache = function() {
            var i, e, t, offset, oOffset;
            for (i = 0; i < targetBounds.length; i += 1) {
                t = targetBounds[i];
                e = t.element;
                offset = e.offset();
                t.x = offset.left;
                t.y = offset.top;
                t.width = e.outerWidth();
                t.height = e.outerHeight();


                //update snapped in draggables too
                if (t.occupied) {


                    var relx = parseInt(t.occupied.css('left')) || 0;
                    var rely = parseInt(t.occupied.css('top')) || 0;

                    oOffset = {
                        top: t.occupied.offset().top - rely,
                        left: t.occupied.offset().left - relx
                    };


                    t.occupied.css('left', offset.left - oOffset.left + (t.element.outerWidth() - $(t.occupied).outerWidth()) / 2);
                    t.occupied.css('top', offset.top - oOffset.top + (t.element.outerHeight() - $(t.occupied).outerHeight()) / 2);

                    /*                    left: targetOffset.left - draggingOriginalOffset.left + (targetElement.outerWidth() - dragElement.outerWidth()) / 2,
                    top: targetOffset.top - draggingOriginalOffset.top + (targetElement.outerHeight() - dragElement.outerHeight()) / 2
*/
                }
            }

        };

        var stopDrag = function(eo) {
            var i, allTargetsOccupied = true;
            eo.preventDefault();
            //final positioning of element
            if (currentTarget) {


                //set target to match height of dragged item if
                //specified in setttings.
                if (settings.matchSizeOnDrop) {
                    currentTarget.element.width(dragging.width());
                    currentTarget.element.height(dragging.height());
                    //update collision detection cache with new dimensions and positions
                    resetTargetCache();
                }


                //add item to target
                snapToTarget(dragging, currentTarget.element);
                currentTarget.occupied = dragging;

                //check if all targets are in place 
                for (i = 0; i < targetBounds.length; i += 1) {
                    if (!targetBounds[i].occupied) {
                        allTargetsOccupied = false;
                        break;
                    }
                }

                if ((settings.instantReturn || allTargetsOccupied) && settings.automatic) {
                    sendBack();
                }

                if (allTargetsOccupied) {
                    o.trigger('complete');
                }


            } else {
                dragging.animate({
                    top: 0,
                    left: 0
                }, 250, function() {
                    $(this).css('position', 'inherit');
                    $(this).removeClass(settings.draggingClass);
                    $(this).css('zIndex', '');
                    if (settings.matchSizeOnDrop) {
                        resetTargetCache();
                    }
                });
            }

            //clean up over target
            if (currentTarget) {
                currentTarget.element.removeClass(settings.dragOverClass);
                currentTarget = null;
            }

            //set dragging to null to stop dragUpdate requesting another animation frame

            dragging = null;
            dragPos = null;
            doc.off('mousemove touchmove', dragMonitor);
            doc.off('mouseup touchend', stopDrag);
        };

        //record mouse / finger position with mousemove event
        //dragUpdate coupled with requestAnimationFrame does actually dom / repaint operations 
        //for best performance
        var dragMonitor = function(eo) {
            var box, target, offset;

            eo.preventDefault();

            //capture current mouse position
            dragPos = getPosition(eo);

            //get offset of element being dragged
            offset = dragging.offset();

            //create box object ready for collision detection
            box = {
                x: offset.left,
                y: offset.top,
                width: draggingWidth,
                height: draggingHeight
            };

            //collision detedc
            target = overTarget(box);


            if (target && !target.occupied) {
                //over a target and its free to occupy
                if (target != currentTarget) {
                    //not over it alrady
                    //add designer class to target
                    if (settings.dragOverClass) {
                        target.element.addClass(settings.dragOverClass);
                    }

                    //remove designer class from existing target
                    if (currentTarget) {
                        currentTarget.element.removeClass(settings.dragOverClass);
                    }
                    //new becomes existing
                    currentTarget = target;


                }
            } else {

                //not over anything remove class and set target to null
                if (currentTarget) {
                    currentTarget.element.removeClass(settings.dragOverClass);
                }

                currentTarget = null;
            }

        };

        var dragUpdate = function() {
            //update position
            if (dragging) {
                //check if drag has occurred first
                if (dragPos) {
                    dragging.css('left', (dragPos.x - draggingOffset.left - draggingOriginalOffset.left) + 'px');
                    dragging.css('top', (dragPos.y - draggingOffset.top - draggingOriginalOffset.top) + 'px');
                }

                requestAnimationFrame(dragUpdate);
            }
        };


        var setupDrag = function(element) {
            var jqe = $(element);
            jqe.on('mousedown touchstart', startDrag);
        };


        var setupTarget = function(element) {
            var offset, jqe = $(element);

            //create a boundbox for use in collision detection later
            offset = jqe.offset();

            return {
                x: offset.left,
                y: offset.top,
                width: jqe.outerWidth(),
                height: jqe.outerHeight(),
                element: jqe,
                occupied: false
            };
        };

        var overTarget = function(box) {
            var i, target;
            for (i = 0; i < targetBounds.length; i += 1) {
                target = targetBounds[i];
                if (boxCollision(box, target)) {
                    return target;
                }
            }

            return false;
        };

        /**
         * Check if boxes intersect, define object box as having properties of
         * x, y, width and height
         * @param  {object} a [description]
         * @param  {object} b [description]
         * @return {boolean} true if boxes intersect
         */
        var boxCollision = function(a, b) {
            return (a.x + a.width >= b.x &&
                a.x <= b.x + b.width &&
                a.y + a.height >= b.y &&
                a.y <= b.y + b.height);
        };

        var reset = function() {
            var target, i;
            for (i = 0; i < targetBounds.length; i += 1) {
                target = targetBounds[i];
                if (target.occupied) {
                    target.occupied.animate({
                        top: 0,
                        left: 0
                    }, 250, function() {
                        $(this).css('position', 'inherit');
                    });

                    target.occupied.removeClass(settings.markCorrectClass);

                    target.occupied = false;

                    if (settings.matchSizeOnDrop) {
                        target.element.css('width', '');
                        target.element.css('height', '');
                    }
                }
            }
            dragging = null;

            if (settings.matchSizeOnDrop) {
                resetTargetCache();
            }
        };


        var showCorrect = function() {
            var dragger, target, i, j, targetOffset, snap, relx, rely, offsetNow, draggingOriginalOffset;
            for (i = 0; i < draggables.length; i += 1) {
                dragger = $(draggables[i]);
                //find the correct target
                for (j = 0; j < targetBounds.length; j += 1) {
                    target = targetBounds[j];
                    if (dragger.attr('data-dragdrop-id') === target.element.attr('data-dragdrop-accepts')) {
                        targetOffset = target.element.offset();
                        offsetNow = dragger.offset();

                        relx = parseInt(dragger.css('left')) || 0;
                        rely = parseInt(dragger.css('top')) || 0;

                        draggingOriginalOffset = {
                            top: offsetNow.top - rely,
                            left: offsetNow.left - relx
                        };

                        if (settings.matchSizeOnDrop) {
                            target.element.width(dragger.width());
                            target.element.height(dragger.height());
                            //update collision detection cache with new dimensions and positions
                            resetTargetCache();
                        }

                        snap = {
                            left: targetOffset.left - draggingOriginalOffset.left + (target.element.outerWidth() - dragger.outerWidth()) / 2,
                            top: targetOffset.top - draggingOriginalOffset.top + (target.element.outerHeight() - dragger.outerHeight()) / 2
                        };




                        dragger.css('position', 'relative');
                        target.occupied = dragger;
                        dragger.animate(snap, 250, function() {
                            $(this).removeClass(settings.draggingClass);

                            $(this).css('zIndex', '');
                            resetTargetCache();

                        });
                    }

                }

            }



        };


        var sendBack = function() {
            var target, match, acceptableids, i, j, allCorrect = true;
            for (i = 0; i < targetBounds.length; i += 1) {
                target = targetBounds[i];
                if (target.occupied) {

                    //check if the dragged in item matches any of the acceptable ids;
                    acceptableids = target.element.attr('data-dragdrop-accepts').split(' ');
                    match = false;
                    for (j = 0; j < acceptableids.length; j += 1) {
                        if (target.occupied.attr('data-dragdrop-id') === acceptableids[j]) {
                            match = true;
                            if (settings.markCorrectClass) {
                                target.occupied.addClass(settings.markCorrectClass);
                            }
                            break;
                        }
                    }

                    if (!match) {
                        //no match send it back
                        target.occupied.animate({
                            top: 0,
                            left: 0
                        }, 250, function() {
                            $(this).css('position', 'inherit');
                        });
                        allCorrect = false;
                        target.occupied = false;
                        if (settings.matchSizeOnDrop) {
                            target.element.css('width', '');
                            target.element.css('height', '');
                        }
                    }
                }
            }

            if (settings.matchSizeOnDrop) {
                resetTargetCache();
            }

            return allCorrect;
        };

        var snapToTarget = function(dragElement, targetElement) {
            var targetOffset = targetElement.offset();
            var dragTo

            if (settings.snapTo == 'left') {
                dragTo = {
                    left: targetOffset.left - draggingOriginalOffset.left,
                    top: targetOffset.top - draggingOriginalOffset.top + (targetElement.outerHeight() - dragElement.outerHeight()) / 2
                }
            } else {
                dragTo = {
                    left: targetOffset.left - draggingOriginalOffset.left + (targetElement.outerWidth() - dragElement.outerWidth()) / 2,
                    top: targetOffset.top - draggingOriginalOffset.top + (targetElement.outerHeight() - dragElement.outerHeight()) / 2
                };
            }

            dragElement.animate(dragTo, 50, function() {
                $(this).removeClass(settings.draggingClass);
                $(this).css('zIndex', '');

                if (settings.matchSizeOnDrop) {
                    resetTargetCache();
                }

                o.trigger('snap', {
                    dragElement: dragElement,
                    targetElement: targetElement
                });

            });
        };

        //extract x an y coordinates of mouse or finger
        var getPosition = function(eo) {
            var position;

            if (eo.type === 'touchstart' || eo.type === 'touchmove') {
                position = {
                    x: eo.originalEvent.changedTouches[0].pageX,
                    y: eo.originalEvent.changedTouches[0].pageY
                };
            } else if (eo.type === 'mousedown' || eo.type === 'mousemove') {
                position = {
                    x: eo.pageX,
                    y: eo.pageY
                };
            }

            return position;

        };

        o = {

            init: function() {
                var i;

                targets = $('#' + containerId + '  [data-dragdrop-accepts]');
                draggables = $('#' + containerId + ' [data-dragdrop-id]');
                targetBounds = [];


                //add drag to each draggable
                for (i = 0; i < draggables.length; i += 1) {
                    setupDrag(draggables[i]);
                }

                //setup each target
                for (i = 0; i < targets.length; i += 1) {
                    targetBounds.push(setupTarget(targets[i]));
                }

            },

            reset: function() {
                answersHaveBeenChecked = false;
                reset();
            },

            checkAnswers: function() {
                answersHaveBeenChecked = true;
                return sendBack();
            },

            showAnswers: function() {
                showCorrect();
            },

            bind: function(event, fct) {
                this._events = this._events || {};
                this._events[event] = this._events[event] || [];
                this._events[event].push(fct);
            },

            unbind: function(event, fct) {
                this._events = this._events || {};
                if (event in this._events === false) return;
                this._events[event].splice(this._events[event].indexOf(fct), 1);
            },

            trigger: function(event /* , args... */ ) {
                this._events = this._events || {};
                if (event in this._events === false) return;
                for (var i = 0; i < this._events[event].length; i++) {
                    this._events[event][i].apply(this, Array.prototype.slice.call(arguments, 1));
                }
            }



        };

        o.init();

        return o;

    };

    return ns;


}(epg || {}));