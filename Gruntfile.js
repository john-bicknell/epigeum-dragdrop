/*global module*/

module.exports = function(grunt) {
    'use strict';
    // Configuration goes here
    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),
        // Configure the copy task to move files from the development to production folders
        clean: ['prod'],


        uglify: {
            options: {
                banner: '/* <%= pkg.name %> v<%= pkg.version %>| <%=pkg.author%> |<%= grunt.template.today("yyyy-mm-dd") %> */\n'
            },
            my_target: {
                files: [{
                    'prod/epg-dragdrop.js': ['dev/epg-dragdrop.js']
                }]
            }
        },


        jsdoc: {
            dist: {
                src: ['dev/*.js'],
                options: {
                    destination: 'docs'
                }
            }
        },

    });


    // Load plugins here
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-jsdoc');

    // Define your tasks here
    grunt.registerTask('default', ['clean', 'uglify']);

};
