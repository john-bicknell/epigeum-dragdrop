#Drag and Drop for Epigeum

Generic drag and drop designed to be easliy styled and adapted to variations of this type of activity.

Makes use of requestAnimationFrame, so a shim must be applied for older browsers, this is demonstrated in the example directory.

##Usage

Drag items and drag targets can be any dom element, mark an element as a draggable item with the attribute:
	
	data-dragdrop-id="theitemdragid"

Then mark it's correspondig target with, 

	data-dragdrop-accepts="theitemdragid"

Targets can accept more than one item by using a space separated list in the attribute:

	data-dragdrop-accepts="theitemdragid anotheritemid andanotherid"

Note the id is arbitary, nothing to do with the standard dom element id.

Place all the targets and draggable items within in one div, then pass the dom id of that div to the drag drop javascript code,
along with any required options: 


	var dragdrop = epg.DragDrop('stat-drag', {
		draggingClass: 'statDragging',
	    dragOverClass: 'statOver',
	    automatic: false,
	    matchSizeOnDrop: true
	} , jQuery);

##Options

*	automatic - When all items are dragged to a target the incorrectly placed ones are returned to their start position automatically.
*	matchSizeOnDrop - Target takes shape of dropped on item.
*	draggingClass - Name of css class to be applied to element being dragged around.
* 	dragOverClass - Name of css class to be applied to target element that can accept the dragged item.

For more info look at the examples directory.

##API 

Reset the activity:

	dragdrop.reset();

Move incorrectly placed items back to their start postions:

	dragdrop.checkAnswers();
	
Position all items into their correct targets:

	dragdrop.showAnswers();